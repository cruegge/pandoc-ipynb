{ mkDerivation, aeson, base, base64-bytestring, bytestring
, containers, mtl, pandoc, pandoc-types, stdenv, text, yaml
, lib, jq
}:
mkDerivation {
  pname = "pandoc-ipynb";
  version = "0.1.0.0";
  src = lib.cleanSourceWith {
    filter = name: type: builtins.match "\\..*" (baseNameOf name) == null;
    src = lib.cleanSource ./.;
  };
  isLibrary = false;
  isExecutable = true;
  # enableSharedExecutables = false;
  executableHaskellDepends = [
    aeson base base64-bytestring bytestring containers mtl pandoc
    pandoc-types text yaml
  ];
  license = stdenv.lib.licenses.mit;
  hydraPlatforms = stdenv.lib.platforms.none;
  postInstall = ''
    cp bin/mdconvert $out/bin/mdconvert
    sed -i $out/bin/mdconvert \
      -e "s|^PANDOC_IPYNB=.*|PANDOC_IPYNB=$out/bin/pandoc-ipynb|" \
      -e "s|^JQ=.*|JQ=${jq}/bin/jq|"
  '';
}
