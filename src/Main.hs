module Main where

import Control.Arrow ((***))
import Control.Monad.Except (throwError)
import Data.Aeson ((.=))
import qualified Data.Aeson as A
import Data.Aeson.Types (emptyArray)
import Data.Bifunctor (bimap)
import qualified Data.ByteString.Base64.Lazy as B64
import qualified Data.ByteString.Lazy as BL
import Data.Either (partitionEithers)
import Data.Foldable (foldrM)
import Data.List (intercalate)
import qualified Data.Map as M
import Data.Maybe (fromMaybe, mapMaybe)
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import qualified Data.Text.IO as TIO
import qualified Data.Text.Lazy as L
import qualified Data.Text.Lazy.Encoding as LE
import qualified Data.Yaml as Y
import Text.Pandoc (def, runIOorExplode)
import Text.Pandoc.Class
  ( PandocMonad
  , fetchMediaResource
  , getMediaBag
  , insertMedia
  )
import Text.Pandoc.Definition
  ( Block(CodeBlock, Div, RawBlock)
  , Format(Format)
  , Inline(Image)
  , Pandoc(Pandoc)
  , nullMeta
  )
import Text.Pandoc.Error (PandocError(PandocParseError))
import Text.Pandoc.Extensions
  ( Extension(Ext_fenced_divs, Ext_tex_math_dollars)
  , enableExtension
  , githubMarkdownExtensions
  , pandocExtensions
  )
import Text.Pandoc.MediaBag (lookupMedia, mediaDirectory)
import Text.Pandoc.Options (readerExtensions, writerExtensions, writerSetextHeaders)
import Text.Pandoc.Readers.Markdown (readMarkdown)
import Text.Pandoc.Walk (walkM)
import Text.Pandoc.Writers.Markdown (writeMarkdown)

main :: IO ()
main = do
  input <- TIO.getContents
  notebook <-
    runIOorExplode $ do
      Pandoc _ blocks <- readPandocMarkdown input
      blocksToNotebook blocks
  BL.putStr $ A.encode notebook

readPandocMarkdown :: PandocMonad m => T.Text -> m Pandoc
readPandocMarkdown =
  readMarkdown
    def {readerExtensions = enableExtension Ext_fenced_divs pandocExtensions}

writeGfm :: PandocMonad m => Pandoc -> m T.Text
writeGfm = writeMarkdown def
  { writerExtensions = enableExtension Ext_tex_math_dollars githubMarkdownExtensions
  , writerSetextHeaders = False
  }

data Notebook = Notebook
  { nbmeta :: Metadata
  , nbcells :: [Cell]
  }

data Cell = Cell
  { celltype :: CellType
  , cellmeta :: Metadata
  , cellsource :: T.Text
  }

data CellType
  = MarkdownCell { attachments :: Attachments }
  | CodeCell

type Metadata = M.Map String A.Value

newtype Attachments =
  Attachments (M.Map FilePath (String, BL.ByteString))

instance A.ToJSON Notebook where
  toJSON nb =
    A.object
      [ T.pack "nbformat" .= (4 :: Int)
      , T.pack "nbformat_minor" .= (2 :: Int)
      , T.pack "metadata" .= nbmeta nb
      , T.pack "cells" .= nbcells nb
      ]

instance A.ToJSON Cell where
  toJSON cell@Cell {celltype = MarkdownCell {attachments = att}} =
    A.object
      [ T.pack "cell_type" .= T.pack "markdown"
      , T.pack "metadata" .= cellmeta cell
      , T.pack "source" .= cellsource cell
      , T.pack "attachments" .= att
      ]
  toJSON cell@Cell {celltype = CodeCell} =
    A.object
      [ T.pack "cell_type" .= T.pack "code"
      , T.pack "execution_count" .= A.Null
      , T.pack "metadata" .= cellmeta cell
      , T.pack "source" .= cellsource cell
      , T.pack "outputs" .= emptyArray
      ]

instance A.ToJSON Attachments where
  toJSON (Attachments att) =
    A.toJSON . flip M.map att $ \(mimetype, bytes) ->
      A.object [T.pack mimetype .= toBase64 bytes]

toBase64 :: BL.ByteString -> L.Text
toBase64 = LE.decodeUtf8 . B64.encode

blocksToNotebook :: PandocMonad m => [Block] -> m Notebook
blocksToNotebook = protoToNotebook . blocksToProto

type ProtoNotebook = (String, [ProtoCell])

data ProtoCell
  = ProtoMarkdown [Block]
  | ProtoCode String
  | ProtoMeta String

blocksToProto :: [Block] -> ProtoNotebook
blocksToProto =
  (intercalate "\n" *** makecells) . partitionEithers . mapMaybe convert
  where
    convert :: Block -> Maybe (Either String (Either Block ProtoCell))
    convert (RawBlock (Format f) raw)
      | f == "ipynb" = Just . Left $ raw
      | f == "ipycell" = Just . Right . Right . ProtoMeta $ raw
      | otherwise = Nothing
    convert block@(CodeBlock (_, classes, _) source)
      | "cell" `elem` classes = Just . Right . Right . ProtoCode $ source
      | otherwise = Just . Right . Left $ block
    convert block@(Div (_, classes, _) blocks)
      | "cell" `elem` classes = Just . Right . Right . ProtoMarkdown $ blocks
      | otherwise = Just . Right . Left $ block
    convert block = Just . Right . Left $ block
    --
    makecells :: [Either Block ProtoCell] -> [ProtoCell]
    makecells = concat . map (either (return . ProtoMarkdown) id) . cluster

cluster :: [Either a b] -> [Either [a] [b]]
cluster = foldr prepend []
  where
    prepend (Left a) (Left as:xs) = (Left $ a : as) : xs
    prepend (Right b) (Right bs:xs) = (Right $ b : bs) : xs
    prepend x xs = bimap return return x : xs

protoToNotebook :: PandocMonad m => ProtoNotebook -> m Notebook
protoToNotebook (meta, cells) = do
  _nbmeta <- parseMetadata meta
  _nbcells <- foldrM push [] cells
  return Notebook {nbmeta = _nbmeta, nbcells = _nbcells}
  where
    push (ProtoCode s) cs = fmap (: cs) $ makeCodeCell s
    push (ProtoMarkdown bs) cs = fmap (: cs) $ makeMdCell bs
    push (ProtoMeta _) [] = return []
    push (ProtoMeta m) (c:cs) =
      fmap ((: cs) . updateMetadata c) $ parseMetadata m

parseMetadata :: PandocMonad m => String -> m Metadata
parseMetadata s =
  case Y.decodeEither' . TE.encodeUtf8 . T.pack $ s of
    Right m -> return m
    Left e -> throwError . PandocParseError . Y.prettyPrintParseException $ e

updateMetadata :: Cell -> Metadata -> Cell
updateMetadata cell newmeta
  | M.null $ cellmeta cell = cell {cellmeta = newmeta}
  | otherwise = cell

makeCodeCell :: PandocMonad m => String -> m Cell
makeCodeCell source =
  return
    Cell {celltype = CodeCell, cellmeta = M.empty, cellsource = T.pack source}

makeMdCell :: PandocMonad m => [Block] -> m Cell
makeMdCell blocks = do
  source <- writeGfm . Pandoc nullMeta =<< walkM attachImage blocks
  bag <- getMediaBag
  let att =
        Attachments . M.fromList . flip map (mediaDirectory bag) $ \(name, _, _) ->
          let (mimetype, bytes) =
                fromMaybe (error "reached unreachable code") $
                lookupMedia name bag
           in (name, (mimetype, bytes))
  return
    Cell
      { celltype = MarkdownCell {attachments = att}
      , cellmeta = M.empty
      , cellsource = source
      }

attachImage :: PandocMonad m => Inline -> m Inline
attachImage img@(Image attr label (src, title)) = do
  bag <- getMediaBag
  case lookupMedia src bag of
    Just (_, _) -> return img
    Nothing -> do
      (fname, mimeType, bytes) <- fetchMediaResource src
      insertMedia fname mimeType bytes
      return $ Image attr label ("attachment:" ++ fname, title)
attachImage inl = return inl
